package ee.dmf.hackfurby.configuration;

import ee.dmf.hackfurby.resource.FurbyResource;
import io.swagger.v3.jaxrs2.integration.resources.OpenApiResource;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationPath("rest/")
public class AppConfig extends Application {
    @Override
    public Set<Class<?>> getClasses() {
        return Stream.of(FurbyResource.class, OpenApiResource.class).collect(Collectors.toSet());
    }
}
