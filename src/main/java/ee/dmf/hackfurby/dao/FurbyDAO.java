package ee.dmf.hackfurby.dao;

import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.DatastoreOptions;
import com.google.cloud.datastore.KeyFactory;
import com.google.cloud.datastore.StructuredQuery;
import com.google.cloud.datastore.Query;
import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.Key;
import ee.dmf.hackfurby.model.Furby;
import java.util.Iterator;

public class FurbyDAO {
    private final Datastore datastore = DatastoreOptions.getDefaultInstance().getService();
    private final KeyFactory keyFactory = datastore.newKeyFactory().setKind("Furby");

    public Iterator<Entity> findAll() {
        Query<Entity> query =
                Query.newEntityQueryBuilder().setKind("Furby").setOrderBy(StructuredQuery.OrderBy.asc("hacked")).build();
        return datastore.run(query);
    };


    public Furby addFurby() {
        Furby furby = new Furby();
        Entity furbyEntity = toEntity(furby);
        furby.setId(furbyEntity.getKey().getId());

        datastore.put(furbyEntity);
        return furby;
    };

    public void updateFurby(Furby furby) {
        datastore.put(toEntity(furby));
    }

    public Furby findFurbyById(long furbyId) {
        return toFurby(datastore.get(keyFactory.newKey(furbyId)));
    };


    public long deleteFurby(long furbyId) {
        if(findFurbyById(furbyId) != null) {
            datastore.delete(keyFactory.newKey(furbyId));
            return furbyId;
        } else {
            throw new NullPointerException();
        }
    };

    private Entity toEntity (Furby furby) {
        Key key = datastore.allocateId(keyFactory.newKey());
        Entity furbyEntity = Entity.newBuilder(key)
                .set("hacked", furby.isHacked())
                .set("coreEquation", furby.getCoreEquation())
                .set("answer", furby.getAnswer())
                .build();
        return furbyEntity;
    }

    private Furby toFurby (Entity entity) {
        return new Furby(
                entity.getKey().getId(),
                entity.getBoolean("hacked"),
                entity.getString("coreEquation"),
                entity.getDouble("answer")
        );
    }

}
