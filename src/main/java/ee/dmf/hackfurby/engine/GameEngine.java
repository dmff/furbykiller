package ee.dmf.hackfurby.engine;

// Simple game engine example
public class GameEngine {
    private static GameEngine instance;

    public static GameEngine getInstance() {
        if (instance == null) {
            instance = new GameEngine();
        }
        return instance;
    }

    private int randomBetween(int low, int high){
        return (int) Math.round(Math.random() * (high - low)) + low;
    }

    public String [] generateProblem(){
        int a = randomBetween(1, 9);
        int b = randomBetween(1, 9);
        String [] question = new String[2];
        double result;
        char operator;
        switch(randomBetween(1,4)){
            case 1:
                operator = '+';
                result = a + b;
                break;
            case 2:
                operator = '-';
                result = a - b;
                break;
            case 3:
                operator = '*';
                result = a * b;
                break;
            case 4:
                operator = '/';
                result = (double) a / b;
                break;
            default:
                operator = '\0';
                result = Double.NaN;
        }
        question[0] = String.format("%d %c %d = ?",a,operator,b);
        question[1] = String.valueOf(result);
        return question;
    }

    public boolean checkAnswer(String question, double answer) {
        int a = Character.getNumericValue(question.charAt(0));
        int b = Character.getNumericValue(question.charAt(4));

        switch (question.charAt(2)) {
            case '+':
                return (a+b == answer);
            case '-':
                return (a-b == answer);
            case '*':
                return (a*b == answer);
            case '/':
                return ((double)a/b == answer);
        }
        return false;
    }

}
