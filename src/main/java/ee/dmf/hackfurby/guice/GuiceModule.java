package ee.dmf.hackfurby.guice;

import com.google.inject.Binder;
import com.google.inject.Module;
import ee.dmf.hackfurby.resource.FurbyResource;
import ee.dmf.hackfurby.service.FurbyService;

public class GuiceModule implements Module {
    public void configure(final Binder binder)
    {
        binder.bind(FurbyService.class);
        binder.bind(FurbyResource.class);
    }
}
