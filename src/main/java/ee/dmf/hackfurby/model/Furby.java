package ee.dmf.hackfurby.model;

import ee.dmf.hackfurby.engine.GameEngine;

public class Furby {
    private GameEngine gameEngine = GameEngine.getInstance();
    private long id;
    private boolean hacked;
    private String coreEquation;
    private double answer;

    public Furby() {
        String [] problem = gameEngine.generateProblem();

        setHacked(false);
        setCoreEquation(problem[0]);
        setAnswer(Double.valueOf(problem[1]));
    }

    public Furby(long id, boolean hacked, String coreEquation, double answer) {
        setId(id);
        setHacked(hacked);
        setCoreEquation(coreEquation);
        setAnswer(answer);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isHacked() {
        return hacked;
    }

    public void setHacked(boolean hacked) {
        this.hacked = hacked;
    }

    public String getCoreEquation() {
        return coreEquation;
    }

    public void setCoreEquation(String coreEquation) {
        this.coreEquation = coreEquation;
    }

    public double getAnswer() {
        return answer;
    }

    public void setAnswer(double answer) {
        this.answer = answer;
    }
}
