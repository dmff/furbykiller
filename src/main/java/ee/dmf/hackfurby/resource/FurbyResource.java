package ee.dmf.hackfurby.resource;

import com.google.inject.Inject;
import ee.dmf.hackfurby.service.FurbyService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.jboss.resteasy.annotations.jaxrs.PathParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.DELETE;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/api/v1")
@OpenAPIDefinition(info = @Info(
        title = "Furby Resource",
        version = "0.0",
        description = "Sample API"
))
public class FurbyResource {
    @Inject
    private FurbyService furbyService;

    public FurbyResource() {}

    @Path("/all")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get list of Furbies",
            responses = {
                    @ApiResponse(description = "List of furbies",
                            content = @Content(mediaType = "application/json"))})
    public Response getListOfFurbies() {
        return Response.ok().entity(furbyService.findAll()).build();
    }

    @Path("/info")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get info",
            responses = {
                    @ApiResponse(description = "Bit of information about everything")})
    public Response getInfo() {
        return Response.ok().entity("Each time you make a correct answer a furby's being hacked").build();
    }

    @Path("{furbyId}/task")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get specific task to hack furby by furby id",
            parameters = {
                    @Parameter(in = ParameterIn.PATH,
                            name = "furbyId",
                            description = "Id of a furby",
                            example = "5631986051842048",
                            required = true)
            },
            responses = {
                    @ApiResponse(responseCode = "200", description = "Furby's core equation to solve",
                            content = @Content(mediaType = "application/json")),
                    @ApiResponse(responseCode = "404", description = "No such furby with that id in your disposal"),
                    @ApiResponse(responseCode = "400", description = "Illegal argument: please check your input")})
    public Response getTask(@PathParam("furbyId") long furbyId) {
        try {
            return Response.ok().entity(furbyService.findeFurbyById(furbyId).getCoreEquation()).build();
        } catch (NullPointerException e) {
            e.printStackTrace();
            return Response.status(404).entity("No such furby with that id in your disposal").build();
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
            return Response.status(400).entity("Illegal argument: please check your input").build();
        }
    }

    @Path("/solve/{furbyId}/{answer}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Hack furby by passing furbyId and answer",
            parameters = {
                    @Parameter(in = ParameterIn.PATH,
                            name = "furbyId",
                            description = "Id of a furby",
                            example = "5631986051842048",
                            required = true),
                    @Parameter(in = ParameterIn.PATH,
                            name = "answer",
                            description = "Number which is the answer to furby's core equation",
                            example = "15",
                            required = true)
            },
            responses = {
                    @ApiResponse(responseCode = "200", description = "The result (true/false)",
                            content = @Content(mediaType = "application/json")),
                    @ApiResponse(responseCode = "404", description = "No such furby with that id in your disposal"),
                    @ApiResponse(responseCode = "400", description = "Illegal argument: please check your input")})
    public Response solveTask(@PathParam("furbyId") long furbyId, @PathParam("answer") double answer) {
        try {
            return Response.ok().entity(furbyService.hackFurby(furbyId, answer)).build();
        } catch (NullPointerException e) {
            e.printStackTrace();
            return Response.status(404).entity("No such furby with that id in your disposal").build();
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
            return Response.status(400).entity("Illegal argument: please check your input").build();
        }
    }

    @Path("/create")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Create new furby",
            responses = {
                    @ApiResponse(responseCode = "200", description = "New furby",
                            content = @Content(mediaType = "application/json"))})
    public Response generateFurby() {
        return Response.ok().entity(furbyService.addFurby()).build();
    }

    @Path("/delete/{furbyId}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Removing furby from a database",
            parameters = {
                    @Parameter(in = ParameterIn.PATH,
                            name = "furbyId",
                            description = "Id of a furby",
                            example = "5631986051842048",
                            required = true)
            },
            responses = {
                    @ApiResponse(responseCode = "200", description = "Furby Id which was removed",
                            content = @Content(mediaType = "application/json")),
                    @ApiResponse(responseCode = "404", description = "No such furby with that id in your disposal")})
    public Response deleteFurby(@PathParam("furbyId") long furbyId) {
        try {
            return Response.ok().entity(furbyService.deleteFurby(furbyId)).build();
        } catch (NullPointerException e) {
            e.printStackTrace();
            return Response.status(404).entity("No such furby with that id in your disposal").build();
        }
    }
}
