package ee.dmf.hackfurby.service;

import com.google.cloud.datastore.Entity;
import com.google.inject.Inject;
import ee.dmf.hackfurby.dao.FurbyDAO;
import ee.dmf.hackfurby.engine.GameEngine;
import ee.dmf.hackfurby.model.Furby;
import java.util.ArrayList;
import java.util.List;

public class FurbyService {
    @Inject
    private FurbyDAO furbyDAO;
    private GameEngine gameEngine = GameEngine.getInstance();

    public FurbyService() {}

    public List<Furby> findAll() {
        ArrayList<Furby> furbiesList = new ArrayList<>();
        furbyDAO.findAll().forEachRemaining(e -> furbiesList.add(toFurby(e)));
        return furbiesList;
    }

    public Furby findeFurbyById(long furbyId) {
        return furbyDAO.findFurbyById(furbyId);
    }

    public boolean hackFurby(long furbyId, double answer) {
        Furby furby = furbyDAO.findFurbyById(furbyId);
        if (gameEngine.checkAnswer(furby.getCoreEquation(), answer)) {
            furby.setHacked(true);
            furbyDAO.updateFurby(furby);
            return true;
        }
        return false;
    }

    public Furby addFurby() {
        return furbyDAO.addFurby();
    }

    public long deleteFurby(long furbyId) {
        return furbyDAO.deleteFurby(furbyId);
    }

    private Furby toFurby (Entity entity) {
        return new Furby(
                entity.getKey().getId(),
                entity.getBoolean("hacked"),
                entity.getString("coreEquation"),
                entity.getDouble("answer")
        );
    }

}
